package com.chudy.visual.server.interceptor;

import com.chudy.visual.server.service.ScTenantService;
import com.jfinal.aop.Inject;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Record;

import javax.servlet.http.HttpServletRequest;

public class TokenSignInterceptor implements Interceptor {

    @Inject
    ScTenantService scTenantService;

    public void intercept(Invocation invocation) {
        HttpServletRequest request = invocation.getController().getRequest();
        String sessionkey = request.getHeader("sessionkey");
        String imei = request.getHeader("imei");
        System.out.println(sessionkey + "---" + imei );
        Record r = scTenantService.verifyToken(sessionkey,imei);

        if(null != r){
            invocation.getController().setAttr("tenantId",r.getLong("tenant_id"));
            invocation.invoke();
        }else{
            //验证失败
            Ret ret = new Ret();
            ret.put("code",-999);
            ret.put("msg","Invalid token");
            invocation.getController().renderJson(ret);
        }

    }
}
