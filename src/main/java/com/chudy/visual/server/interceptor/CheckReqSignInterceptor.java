package com.chudy.visual.server.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
/**
 * 检查客户端来请求数据的前面，针对获取数据、提交数据等。
 * @author Administrator
 *
 */
public class CheckReqSignInterceptor implements Interceptor {
    public void intercept(Invocation inv) {
       System.out.println("Before method invoking CheckReqSignInterceptor");
       inv.invoke();
       System.out.println("After method invoking");
    }
}