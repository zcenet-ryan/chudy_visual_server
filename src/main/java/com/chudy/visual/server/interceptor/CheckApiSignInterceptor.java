package com.chudy.visual.server.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
/**
 * 检查对外开放api的 前面校验
 * @author Administrator
 *
 */
public class CheckApiSignInterceptor implements Interceptor {
    public void intercept(Invocation inv) {
       System.out.println("Before method invoking");
       inv.invoke();
       System.out.println("After method invoking");
    }
}