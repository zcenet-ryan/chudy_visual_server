package com.chudy.visual.server.plugins;

//import com.baidu.brcc.ConfigItemChangedCallable;
//import com.baidu.brcc.ConfigLoader;
//import com.baidu.brcc.DefaultConfigItemChangedCallable;
//import com.baidu.brcc.model.ChangedConfigItem;
import com.jfinal.plugin.IPlugin;
import org.tio.utils.jfinal.P;

/**
 *lastSheetData = exContent
 * @author hewl
 */
public class BrccStarter implements IPlugin {
	
	public boolean start() {
		try {
			P.use("brcc.properties");
			String ccServerUrl = P.get("rcc.cc-server-url");
			String apiPassword = P.get("rcc.cc-password");
			String projectName = P.get("rcc.project-name");
			String envName = P.get("rcc.env-name");
			String ccVersionName = P.get("rcc.cc-version-name");
			boolean enableUpdateCallback = true;
			long connectionTimeOut=2000l;long readTimeOut=10000l;long callbackInteval=5000l;

			// here to set your parameter
//			BrccConst.configLoader = new ConfigLoader(ccServerUrl,apiPassword,projectName,envName,ccVersionName,enableUpdateCallback,connectionTimeOut,readTimeOut,callbackInteval);
//
//			// get all configuration items by above parameters
//			final Map<String, String> allConfigItems = BrccConst.configLoader.getFromCC();
//
//			// to start update call back
//			ConfigItemChangedCallable callback = new ConfigItemChangedCallable() {
//				public void changed(List<ChangedConfigItem> items) {
//					if (items != null) {
//						for (ChangedConfigItem changedConfigItem : items) {
//							// here can log out
////							System.out.println(changedConfigItem.getKey() + changedConfigItem.getOldValue()
////									+ changedConfigItem.getNewValue());
//
//							// or to update your map
//							//allConfigItems.put(changedConfigItem.getKey(), changedConfigItem.getNewValue());
//							if(null!=BrccConst.configLoader.getRccProperties())
//								BrccConst.configLoader.getRccProperties().setProperty(changedConfigItem.getKey(), changedConfigItem.getNewValue());
//						}
//					}else{
//					}
//				}
//			};
//			BrccConst.configLoader.setChangedCallable(Collections.singletonList(callback));
//			Properties properties = new Properties();
//			properties.putAll(allConfigItems);
//			BrccConst.configLoader.setRccProperties(properties);
//			BrccConst.configLoader.startListening(properties); // start callback listener


//			P.append()
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	public boolean stop() {
		// TODO Auto-generated method stub
		return true;
	}

}