package com.chudy.visual.server.plugins;

import com.chudy.visual.server.websocket.VisualServerConfig;
import org.tio.utils.jfinal.P;

import com.chudy.visual.server.websocket.VisualWebsocketStarter;
import com.chudy.visual.server.websocket.VisualWsMsgHandler;
import com.jfinal.plugin.IPlugin;

/**
 *
 */
public class TioServerStarter implements IPlugin {
	
	public boolean start() {
		// TODO Auto-generated method stub
		System.out.println(TioConst.appStarter);
        
		try {
			P.use("socket.properties");
			TioConst.appStarter = new VisualWebsocketStarter(P.getInt("ws.port", VisualServerConfig.SERVER_PORT), VisualWsMsgHandler.me);
			TioConst.appStarter.getWsServerStarter().start();
			TioConst.wsServerStarter = TioConst.appStarter.getWsServerStarter();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	public boolean stop() {
		// TODO Auto-generated method stub
		return true;
	}

}