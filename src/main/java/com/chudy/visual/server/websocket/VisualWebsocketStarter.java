package com.chudy.visual.server.websocket;

import org.tio.server.ServerGroupContext;
import org.tio.utils.jfinal.P;
import org.tio.websocket.server.WsServerStarter;


/**
 * @author owes.he
 */
public class VisualWebsocketStarter {
	private WsServerStarter wsServerStarter;
	private ServerGroupContext serverGroupContext;

	public VisualWebsocketStarter(int port, VisualWsMsgHandler wsMsgHandler) throws Exception {
		wsServerStarter = new WsServerStarter(port, wsMsgHandler);

		wsServerStarter.getWsServerConfig().setServerInfo("chudy-visual-server");
		serverGroupContext = wsServerStarter.getServerGroupContext();
		serverGroupContext.setName(VisualServerConfig.PROTOCOL_NAME);
		serverGroupContext.setServerAioListener(VisualServerAioListener.me);

		// 设置ip监控
		serverGroupContext.setIpStatListener(VisualIpStatListener.me);
		// 设置ip统计时间
		serverGroupContext.ipStats.addDurations(VisualServerConfig.IpStatDuration.IPSTAT_DURATIONS);
		serverGroupContext.debug = false;
		// 设置心跳超时时间
		serverGroupContext.setHeartbeatTimeout(VisualServerConfig.HEARTBEAT_TIMEOUT);

		if (P.getInt("ws.use.ssl", 1) == 1) {
			// String keyStoreFile = "classpath:config/ssl/keystore.jks";
			// String trustStoreFile = "classpath:config/ssl/keystore.jks";
			// String keyStorePwd = "214323428310224";

			String keyStoreFile = P.get("ssl.keystore", null);
			String trustStoreFile = P.get("ssl.truststore", null);
			String keyStorePwd = P.get("ssl.pwd", null);
			serverGroupContext.useSsl(keyStoreFile, trustStoreFile, keyStorePwd);
		}
	}

	/**
	 * @return the serverGroupContext
	 */
	public ServerGroupContext getServerGroupContext() {
		return serverGroupContext;
	}

	public WsServerStarter getWsServerStarter() {
		return wsServerStarter;
	}

	public static void main(String[] args) throws Exception {
	}
}
