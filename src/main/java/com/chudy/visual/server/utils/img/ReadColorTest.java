package com.chudy.visual.server.utils.img;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class ReadColorTest {
	private Map<String,String> mMap = new HashMap<String,String>();
	private Integer num = 3;
	/**
	 * 读取一张图片的RGB值
	 * 
	 * @throws Exception
	 */
	public void getImagePixel(String image) throws Exception {
		int[] rgb = new int[3];
		File file = new File(image);
		BufferedImage bi = null;
		try {
			bi = ImageIO.read(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		int width = bi.getWidth();
		int height = bi.getHeight();
		int minx = bi.getMinX();
		int miny = bi.getMinY();
		System.out.println("width=" + width + ",height=" + height + ".");
		System.out.println("minx=" + minx + ",miniy=" + miny + ".");
		
		mMap.clear();
		for (int i = minx; i < width; i++) {
			boolean flag = false;
			for (int j = miny; j < height; j++) {
				int pixel = bi.getRGB(i, j); // 下面三行代码将一个数字转换为RGB数字
				rgb[0] = (pixel & 0xff0000) >> 16;
				rgb[1] = (pixel & 0xff00) >> 8;
				rgb[2] = (pixel & 0xff);
				mMap.put(String.valueOf(pixel), String.valueOf(pixel));
				//System.out.println(String.valueOf(pixel) + "=========i=" + i + ",j=" + j + ":(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")");
				if(mMap.size()>= num ) {
					flag = true;
					break;
				}
				//System.out.println(mMap.size());
				//System.out.println("i=" + i + ",j=" + j + ":(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")");
			}
			if(flag) {
				break;
			}
		}
		System.out.println("色值数量：" + mMap.size());
	}

	/**
	 * 返回屏幕色彩值
	 * 
	 * @param x
	 * @param y
	 * @return
	 * @throws AWTException
	 */
	public int getScreenPixel(int x, int y) throws AWTException { // 函数返回值为颜色的RGB值。
		Robot rb = null; // java.awt.image包中的类，可以用来抓取屏幕，即截屏。
		rb = new Robot();
		Toolkit tk = Toolkit.getDefaultToolkit(); // 获取缺省工具包
		Dimension di = tk.getScreenSize(); // 屏幕尺寸规格
		//System.out.println(di.width);
		//System.out.println(di.height);
		Rectangle rec = new Rectangle(0, 0, di.width, di.height);
		BufferedImage bi = rb.createScreenCapture(rec);
		int pixelColor = bi.getRGB(x, y);
		return 16777216 + pixelColor; // pixelColor的值为负，经过实践得出：加上颜色最大值就是实际颜色值。
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		int x = 0;
		ReadColorTest rc = new ReadColorTest();
//		x = rc.getScreenPixel(100, 345);
//		System.out.println(x + " - ");
//		int[] rgb = new int[3];
//		rgb[0] = (x & 0xff0000) >> 16;
//		rgb[1] = (x & 0xff00) >> 8;
//		rgb[2] = (x & 0xff);
//		System.out.println("i=" + ":(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")");
		rc.getImagePixel("d:/timg.jpg");
		//D:\a
		rc.getImagePixel("d:/a/123.png");//读取图片的颜色数量
	}

}