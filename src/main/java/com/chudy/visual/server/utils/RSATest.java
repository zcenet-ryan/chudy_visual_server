package com.chudy.visual.server.utils;

import java.security.KeyPair;
import java.security.PrivateKey;

import org.apache.commons.codec.binary.Hex;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

public class RSATest {

	public static String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJAvSHjFU+G95xB+BbEX6KXKg4GOnRD5o1kyiEe48yQ8ydsYAlUCJ4b/fLEOmThiBKzLsiL0iS4VqZTp77CXfjNybD/kc+94lmQeJJy5zlGqlyVVcLtk5UeestkDPfZsXph/PKMB33x5+mH54UsGtp6RlKLb2PD8MdO2cp7o2MtZAgMBAAECgYAM6O7oQWLdV4+U3nPMA4QFNDjKJObTWkbR+cBSC7FZkUfRojizO9omMEIN6Gy3q68XebWdg5+0zKO+faPEgAwAlNnyoAGg/uV4pHAwqkUo5TmZJXwGdF6ws5t1JNBnHg/WcGcDB5LDtM53hijw3oYFbK8UOcxp4cvSSxBxGNbjwQJBAMJnsZtufArq7ly8xX7r5anP0FXPv7KtshbS+96G2HNB53xKbX1og+BlzvC9KsoJZD/qtkB8EHzYgeTBHXGKmV0CQQC93i/3hBkvnNJVswEtt4elOpOvp3FLu3a3rp7VP2i6gpS7KTQCNIQMfCEOWyM6G4SKWy600BCQjPgr99MUL84tAkEAvXjW74h1OSbT/mq1ighcJG84vAno6jrEMd6cQLvoRh1bghYANJQixBg14whbpeIVZP5bdun0H9ncb5mQGtNcuQJAYm0qRI/QVqRfTVnshHyd7wO8x9RvoSjfVD1LzZR/gCBtZdDoSzRaN/apVD3etyLIxMicH9O0oprRlEl2vQJDSQJASx42U1wrlsiFIrJszi/VMY1inkjr/CQ3U8lUCfrxEkiC5Ji90zP4drs+IxHQgZfZ1/5GC0efL4x2efR6yl8J/g==";
	public static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQL0h4xVPhvecQfgWxF+ilyoOBjp0Q+aNZMohHuPMkPMnbGAJVAieG/3yxDpk4YgSsy7Ii9IkuFamU6e+wl34zcmw/5HPveJZkHiScuc5RqpclVXC7ZOVHnrLZAz32bF6YfzyjAd98efph+eFLBraekZSi29jw/DHTtnKe6NjLWQIDAQAB";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//genRSAkey();
		testEq();
	}

	public static void genRSAkey() {
//		KeyPair pair = SecureUtil.generateKeyPair("RSA");
//		pair.getPrivate();
//		pair.getPublic();
		RSA rsa = new RSA();
		// 获得私钥
		// rsa.getPrivateKey();
		String privateKey = rsa.getPrivateKeyBase64();
		// 获得公钥
		// rsa.getPublicKey();
		String publicKey = rsa.getPublicKeyBase64();
		System.out.println("privateKey=" + privateKey);
		System.out.println("publicKey=" + publicKey);
	}

	public static void testEq() {
		System.out.println(KeyType.PublicKey);
		RSA rsa = new RSA(privateKey,publicKey);
		byte[] encrypt = rsa.encrypt(StrUtil.bytes("12345678894565465465465464564564566", CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
//		String enc = "633577495a1b20889b16c73c99c0e04a5f8315aa82df75851686e58cfe4876c1890bbe883fc1decaeafd7b3c858da73cd51f096065f60c92f31fc1749cb85baaca8f42fee87c62130dfbae5e30302dc99c67f0db9b059cb639bb5d7cd452ac48d61ba275d956319bc3f47ba6436f3b37d7bd148988de9cc849000fc99dc54fbf";
//		encrypt = HexUtil.decodeHex(enc);
		byte[] decrypt = rsa.decrypt(encrypt, KeyType.PrivateKey);
		System.out.println(HexUtil.encodeHexStr(encrypt));
		System.out.println(StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8));
		// Assert..assertEquals("我是一段测试aaaa", StrUtil.str(decrypt,
		// CharsetUtil.CHARSET_UTF_8));

		// 私钥加密，公钥解密
//		byte[] encrypt2 = rsa.encrypt(StrUtil.bytes("我是一段测试aaaa", CharsetUtil.CHARSET_UTF_8), KeyType.PrivateKey);
//		byte[] decrypt2 = rsa.decrypt(encrypt2, KeyType.PublicKey);
		// Assert.assertEquals("我是一段测试aaaa", StrUtil.str(decrypt2,
		// CharsetUtil.CHARSET_UTF_8));
	}
}
