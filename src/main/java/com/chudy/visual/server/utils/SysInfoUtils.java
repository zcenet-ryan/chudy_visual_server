package com.chudy.visual.server.utils;

import com.alibaba.fastjson.JSONObject;
import com.chudy.visual.server.pojo.winclient.Displaysize;
import com.chudy.visual.server.pojo.winclient.Sysinfo;
import com.jfinal.kit.StrKit;

public class SysInfoUtils {
	
	public static String PlatForm_adrirod = "android";
	public static String PlatForm_win = "windows";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static Sysinfo fromWinJson(String sysinfojson) {
		Sysinfo sysinfo = new Sysinfo();
		if (StrKit.notBlank(sysinfojson)) {
			sysinfo = JSONObject.parseObject(sysinfojson, Sysinfo.class);
		}
		if(null== sysinfo){
			sysinfo = new Sysinfo();
		}
		return sysinfo;
	}
	
	public static com.chudy.visual.server.pojo.adrirodclient.Sysinfo fromAdrirodJson(String sysinfojson) {
		com.chudy.visual.server.pojo.adrirodclient.Sysinfo sysinfo = new com.chudy.visual.server.pojo.adrirodclient.Sysinfo();
		if (StrKit.notBlank(sysinfojson)) {
			sysinfo = JSONObject.parseObject(sysinfojson, com.chudy.visual.server.pojo.adrirodclient.Sysinfo.class);
		}
		if(null== sysinfo){
			sysinfo = new com.chudy.visual.server.pojo.adrirodclient.Sysinfo();
		}
		return sysinfo;
	}

	/**
	 * 
	 * @param sysinfojson
	 * @return
	 */
	public static String getScreenResolutionStr(String sysinfojson) {
		Sysinfo sysinfo = fromWinJson(sysinfojson);
		return sysinfo.get_customDevice().getDisplaysize().toString();
	}

	public static Displaysize getScreenResolution(String sysinfojson) {
		Sysinfo sysinfo = fromWinJson(sysinfojson);
		return sysinfo.get_customDevice().getDisplaysize();
	}
	
	public static String getPlatform(String sysinfojson) {
		if(null==sysinfojson) {
			return null;
		}
		int l = sysinfojson.indexOf("\"platform\":\"android\"");
		return l >=0?"android":"windows";
	}

}
