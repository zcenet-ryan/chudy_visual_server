package com.chudy.visual.server.utils.img;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class ColorUtils {
//	private static final String TAG = ColorUtils.class.getSimpleName();
//	// 没有什么用的,做测试的，随时都可以删除
//	private static Map<MyColor, Integer> mMap = new HashMap<MyColor, Integer>();
//
//	/**
//	 * @param resId 资源文件夹中的索引id
//	 * @Description 检查图片资源的颜色占比
//	 * @author 姚旭民
//	 * @date 2019/7/12 16:57
//	 */
////	public static Map<String, String> analyseColor(Context context, int resId) {
////		return analyseColor(getBitmapFormDrawable(getDrawableFromResources(context, resId)));
////	}
//
//	/**
//	 * @param url sd卡中资源的地址
//	 * @Description 检查图片资源的颜色占比
//	 * @author 姚旭民
//	 * @date 2019/7/12 16:57
//	 */
//	public static Map<String, String> analyseColor(String url) {
//		//Log.i(TAG, "analyseColor| ");
//		FileOutputStream out = null;
//		FileInputStream in = null;
//		try {
//			in = new FileInputStream(url);
//			Bitmap bitmap = BitmapFactory.decodeStream(in);
//			// 开始分析颜色
//			return analyseColor(bitmap);
//		} catch (Exception e) {
//			Log.e(TAG, e.toString());
//		} finally {
//			if (out != null) {
//				try {
//					out.flush();
//					out.close();
//				} catch (Exception e) {
//					//Log.e(TAG, e.toString());
//				}
//			}
//
//			if (in != null) {
//				try {
//					in.close();
//				} catch (Exception e) {
//					//Log.e(TAG, e.toString());
//				}
//			}
//		}
//		return null;
//	}
//
//	/**
//	 * @param bitmap 资源对象
//	 * @Description 检查图片资源的颜色占比
//	 * @author 姚旭民
//	 * @date 2019/7/12 16:58
//	 */
//	public static Map<String, String> analyseColor(Bitmap bitmap) throws NullPointerException {
//		try {
//			if (bitmap == null)
//				throw new NullPointerException("bitmap is null.");
//
//			int rows = bitmap.getWidth();
//			int cols = bitmap.getHeight();
//			MyColor key;
//			int r, g, b;
//			for (int x = 0; x < rows; x++) {
//				for (int y = 0; y < cols; y++) {
//					r = Color.red(bitmap.getPixel(x, y));
//					g = Color.green(bitmap.getPixel(x, y));
//					b = Color.blue(bitmap.getPixel(x, y));
//
//					key = MyColor.find(r, g, b);
//					if (!mMap.containsKey(key)) {
//						mMap.put(key, 1);
//					} else {
//						mMap.put(key, mMap.get(key) + 1);
//					}
//				}
//			}
//
//			//Log.i(TAG, "analyseColor| MyColor JackTestMethod mMap的值为 : " + JSON.toJSONString(mMap));
//			return parcentage(rows * cols);
//		} catch (Exception e) {
//			//Log.e(TAG, e.toString());
//		}
//
//		return null;
//	}
//
//	/**
//	 * @return 返回一个保存颜色和占比的 Map<String, Double> 对象
//	 * @Description 计算百分比
//	 * @author 姚旭民
//	 * @date 2019/7/10 19:08
//	 */
//	public static Map<String, String> parcentage(int total) {
//		MyColor temp;
//		Map<String, String> result = new HashMap<String, String>();
//
//		for (Map.Entry<MyColor, Integer> entry : mMap.entrySet()) {
//			temp = entry.getKey();
//			if (temp != null) {
//				temp.setParcent((entry.getValue() * 1.0) / total);
//				//Log.i(TAG, "parcentage| name : " + temp.getName() + ",parcent : " + temp.getParcent() * 100 + "%");
//				result.put(temp.getName(), temp.getParcent() * 100 + "%");
//			}
//		}
//
//		return result;
//	}

//	public static Bitmap getBitmapFormDrawable(Drawable drawable) {
//		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
//				drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
//		Canvas canvas = new Canvas(bitmap);
//		drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
//		// 设置绘画的边界，此处表示完整绘制
//		drawable.draw(canvas);
//		return bitmap;
//	}
//
//	public static Drawable getDrawableFromResources(Context context, int resId) {
//		return context.getResources().getDrawable(resId);
//	}
}