package com.chudy.visual.server.run;

import com.chudy.visual.server.index.*;
import com.chudy.visual.server.plugins.BrccStarter;
import com.chudy.visual.server.plugins.TioServerStarter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;

public class MainConfig extends JFinalConfig {
	
	
	static Prop p;

	public static void main(String[] args) {
		 UndertowServer.start(MainConfig.class);
	}
	
	/**
	 * PropKit.useFirstFound(...) 使用参数中从左到右最先被找到的配置文件
	 * 从左到右依次去找配置，找到则立即加载并立即返回，后续配置将被忽略
	 */
	static void loadConfig() {
		if (p == null) {
			p = PropKit.useFirstFound("application-pro.txt", "application-dev.txt");
		}
	}
	
	static String platform() {
		return PropKit.get("platform.type","single");
	}
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		loadConfig();
		me.setDevMode(p.getBoolean("devMode", false));
		
		/**
		 * 支持 Controller、Interceptor、Validator 之中使用 @Inject 注入业务层，并且自动实现 AOP
		 * 注入动作支持任意深度并自动处理循环注入
		 */
		me.setInjectDependency(true);
		
		// 配置对超类中的属性进行注入
		me.setInjectSuperClass(true);
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/", IndexController.class, "/index");	// 第三个参数为该Controller的视图存放路径
		me.add("/push", PushController.class);
		me.add("/client", ClientController.class);			// 第三个参数省略时默认与第一个参数值相同，在此即为 "/blog"
		me.add("/config", ConfigController.class);
	}
	
	public void configEngine(Engine me) {
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置 druid 数据库连接池插件
		DruidPlugin druidPlugin = new DruidPlugin(p.get("mysql." + platform() + ".jdbcUrl"), p.get("mysql." + platform() + ".user"), p.get("mysql." + platform() + ".password").trim());
		me.add(druidPlugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		
		// 所有映射在 MappingKit 中自动化搞定
		com.chudy.visual.server.model._MappingKit.mapping(arp);//单租户

		
		me.add(arp);
		me.add(new BrccStarter());
		me.add(new TioServerStarter());
	}
	
	public static DruidPlugin createDruidPlugin() {
		loadConfig();
		return new DruidPlugin(p.get("mysql." + platform() + ".jdbcUrl"), p.get("mysql." + platform() + ".user"), p.get("mysql." + platform() + ".password").trim());
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		
	}
}
