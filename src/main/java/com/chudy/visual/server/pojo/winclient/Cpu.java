/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Cpu {

    private String manufacturer;
    private String brand;
    private String vendor;
    private String family;
    private String model;
    private String stepping;
    private String revision;
    private String voltage;
    private String speed;
    private String speedmin;
    private String speedmax;
    private int cores;
    private int physicalCores;
    private int processors;
    private String socket;
    private Cache cache;
    private String flags;
    public void setManufacturer(String manufacturer) {
         this.manufacturer = manufacturer;
     }
     public String getManufacturer() {
         return manufacturer;
     }

    public void setBrand(String brand) {
         this.brand = brand;
     }
     public String getBrand() {
         return brand;
     }

    public void setVendor(String vendor) {
         this.vendor = vendor;
     }
     public String getVendor() {
         return vendor;
     }

    public void setFamily(String family) {
         this.family = family;
     }
     public String getFamily() {
         return family;
     }

    public void setModel(String model) {
         this.model = model;
     }
     public String getModel() {
         return model;
     }

    public void setStepping(String stepping) {
         this.stepping = stepping;
     }
     public String getStepping() {
         return stepping;
     }

    public void setRevision(String revision) {
         this.revision = revision;
     }
     public String getRevision() {
         return revision;
     }

    public void setVoltage(String voltage) {
         this.voltage = voltage;
     }
     public String getVoltage() {
         return voltage;
     }

    public void setSpeed(String speed) {
         this.speed = speed;
     }
     public String getSpeed() {
         return speed;
     }

    public void setSpeedmin(String speedmin) {
         this.speedmin = speedmin;
     }
     public String getSpeedmin() {
         return speedmin;
     }

    public void setSpeedmax(String speedmax) {
         this.speedmax = speedmax;
     }
     public String getSpeedmax() {
         return speedmax;
     }

    public void setCores(int cores) {
         this.cores = cores;
     }
     public int getCores() {
         return cores;
     }

    public void setPhysicalCores(int physicalCores) {
         this.physicalCores = physicalCores;
     }
     public int getPhysicalCores() {
         return physicalCores;
     }

    public void setProcessors(int processors) {
         this.processors = processors;
     }
     public int getProcessors() {
         return processors;
     }

    public void setSocket(String socket) {
         this.socket = socket;
     }
     public String getSocket() {
         return socket;
     }

    public void setCache(Cache cache) {
         this.cache = cache;
     }
     public Cache getCache() {
         return cache;
     }

    public void setFlags(String flags) {
         this.flags = flags;
     }
     public String getFlags() {
         return flags;
     }

}