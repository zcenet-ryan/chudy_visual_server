/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Baseboard {

    private String manufacturer;
    private String model;
    private String version;
    private String serial;
    private String assetTag;
    public void setManufacturer(String manufacturer) {
         this.manufacturer = manufacturer;
     }
     public String getManufacturer() {
         return manufacturer;
     }

    public void setModel(String model) {
         this.model = model;
     }
     public String getModel() {
         return model;
     }

    public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }

    public void setSerial(String serial) {
         this.serial = serial;
     }
     public String getSerial() {
         return serial;
     }

    public void setAssetTag(String assetTag) {
         this.assetTag = assetTag;
     }
     public String getAssetTag() {
         return assetTag;
     }

}