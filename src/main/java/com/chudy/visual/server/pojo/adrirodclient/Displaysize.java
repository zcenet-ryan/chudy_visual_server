/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.adrirodclient;

/**
 * Auto-generated: 2019-07-18 18:7:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Displaysize {

	private int resolutionHeight;
	private double resolutionWidth;
	private double scale;
	private int dpiX;
	private int dpiY;
	private int height;
	private int width;

	public void setResolutionHeight(int resolutionHeight) {
		this.resolutionHeight = resolutionHeight;
	}

	public int getResolutionHeight() {
		return resolutionHeight;
	}

	public void setResolutionWidth(double resolutionWidth) {
		this.resolutionWidth = resolutionWidth;
	}

	public double getResolutionWidth() {
		return resolutionWidth;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	public double getScale() {
		return scale;
	}

	public void setDpiX(int dpiX) {
		this.dpiX = dpiX;
	}

	public int getDpiX() {
		return dpiX;
	}

	public void setDpiY(int dpiY) {
		this.dpiY = dpiY;
	}

	public int getDpiY() {
		return dpiY;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getWidth() {
		return width;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.width + "x" + this.height;
	}

}