/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.adrirodclient;

/**
 * Auto-generated: 2019-07-18 18:7:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class All {

    private int CONNECTION_TYPE;
    private int CONNECTION_UNKNOW;
    private int CONNECTION_NONE;
    private int CONNECTION_ETHERNET;
    private int CONNECTION_WIFI;
    private int CONNECTION_CELL2G;
    private int CONNECTION_CELL3G;
    private int CONNECTION_CELL4G;
    public void setCONNECTION_TYPE(int CONNECTION_TYPE) {
         this.CONNECTION_TYPE = CONNECTION_TYPE;
     }
     public int getCONNECTION_TYPE() {
         return CONNECTION_TYPE;
     }

    public void setCONNECTION_UNKNOW(int CONNECTION_UNKNOW) {
         this.CONNECTION_UNKNOW = CONNECTION_UNKNOW;
     }
     public int getCONNECTION_UNKNOW() {
         return CONNECTION_UNKNOW;
     }

    public void setCONNECTION_NONE(int CONNECTION_NONE) {
         this.CONNECTION_NONE = CONNECTION_NONE;
     }
     public int getCONNECTION_NONE() {
         return CONNECTION_NONE;
     }

    public void setCONNECTION_ETHERNET(int CONNECTION_ETHERNET) {
         this.CONNECTION_ETHERNET = CONNECTION_ETHERNET;
     }
     public int getCONNECTION_ETHERNET() {
         return CONNECTION_ETHERNET;
     }

    public void setCONNECTION_WIFI(int CONNECTION_WIFI) {
         this.CONNECTION_WIFI = CONNECTION_WIFI;
     }
     public int getCONNECTION_WIFI() {
         return CONNECTION_WIFI;
     }

    public void setCONNECTION_CELL2G(int CONNECTION_CELL2G) {
         this.CONNECTION_CELL2G = CONNECTION_CELL2G;
     }
     public int getCONNECTION_CELL2G() {
         return CONNECTION_CELL2G;
     }

    public void setCONNECTION_CELL3G(int CONNECTION_CELL3G) {
         this.CONNECTION_CELL3G = CONNECTION_CELL3G;
     }
     public int getCONNECTION_CELL3G() {
         return CONNECTION_CELL3G;
     }

    public void setCONNECTION_CELL4G(int CONNECTION_CELL4G) {
         this.CONNECTION_CELL4G = CONNECTION_CELL4G;
     }
     public int getCONNECTION_CELL4G() {
         return CONNECTION_CELL4G;
     }

}