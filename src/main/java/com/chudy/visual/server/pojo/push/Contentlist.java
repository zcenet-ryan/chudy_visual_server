/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.push;

/**
 * Auto-generated: 2019-08-01 11:44:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Contentlist {

	private String url;
	private String startdate;
	private String starttime;
	private int duration;
	private String id;
	private String title;
	private String remark;

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getDuration() {
		return duration;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}