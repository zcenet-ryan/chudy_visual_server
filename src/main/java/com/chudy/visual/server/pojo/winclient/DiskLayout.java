/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DiskLayout {

    private String device;
    private String type;
    private String name;
    private String vendor;
    private long size;
    private int bytesPerSector;
    private int totalCylinders;
    private int totalHeads;
    private long totalSectors;
    private long totalTracks;
    private int tracksPerCylinder;
    private int sectorsPerTrack;
    private String firmwareRevision;
    private String serialNum;
    private String interfaceType;
    private String smartStatus;
    public void setDevice(String device) {
         this.device = device;
     }
     public String getDevice() {
         return device;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setVendor(String vendor) {
         this.vendor = vendor;
     }
     public String getVendor() {
         return vendor;
     }

    public void setSize(long size) {
         this.size = size;
     }
     public long getSize() {
         return size;
     }

    public void setBytesPerSector(int bytesPerSector) {
         this.bytesPerSector = bytesPerSector;
     }
     public int getBytesPerSector() {
         return bytesPerSector;
     }

    public void setTotalCylinders(int totalCylinders) {
         this.totalCylinders = totalCylinders;
     }
     public int getTotalCylinders() {
         return totalCylinders;
     }

    public void setTotalHeads(int totalHeads) {
         this.totalHeads = totalHeads;
     }
     public int getTotalHeads() {
         return totalHeads;
     }

    public void setTotalSectors(long totalSectors) {
         this.totalSectors = totalSectors;
     }
     public long getTotalSectors() {
         return totalSectors;
     }

    public void setTotalTracks(long totalTracks) {
         this.totalTracks = totalTracks;
     }
     public long getTotalTracks() {
         return totalTracks;
     }

    public void setTracksPerCylinder(int tracksPerCylinder) {
         this.tracksPerCylinder = tracksPerCylinder;
     }
     public int getTracksPerCylinder() {
         return tracksPerCylinder;
     }

    public void setSectorsPerTrack(int sectorsPerTrack) {
         this.sectorsPerTrack = sectorsPerTrack;
     }
     public int getSectorsPerTrack() {
         return sectorsPerTrack;
     }

    public void setFirmwareRevision(String firmwareRevision) {
         this.firmwareRevision = firmwareRevision;
     }
     public String getFirmwareRevision() {
         return firmwareRevision;
     }

    public void setSerialNum(String serialNum) {
         this.serialNum = serialNum;
     }
     public String getSerialNum() {
         return serialNum;
     }

    public void setInterfaceType(String interfaceType) {
         this.interfaceType = interfaceType;
     }
     public String getInterfaceType() {
         return interfaceType;
     }

    public void setSmartStatus(String smartStatus) {
         this.smartStatus = smartStatus;
     }
     public String getSmartStatus() {
         return smartStatus;
     }

}