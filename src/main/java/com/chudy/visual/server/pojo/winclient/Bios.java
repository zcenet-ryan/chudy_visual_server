/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;
import java.util.Date;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Bios {

    private String vendor;
    private String version;
    private String releaseDate;
    private String revision;
    public void setVendor(String vendor) {
         this.vendor = vendor;
     }
     public String getVendor() {
         return vendor;
     }

    public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }

    public void setReleaseDate(String releaseDate) {
         this.releaseDate = releaseDate;
     }
     public String getReleaseDate() {
         return releaseDate;
     }

    public void setRevision(String revision) {
         this.revision = revision;
     }
     public String getRevision() {
         return revision;
     }

}