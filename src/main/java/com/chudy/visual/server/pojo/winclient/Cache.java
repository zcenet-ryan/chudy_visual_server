/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Cache {

    private int l1d;
    private int l1i;
    private long l2;
    private long l3;
    public void setL1d(int l1d) {
         this.l1d = l1d;
     }
     public int getL1d() {
         return l1d;
     }

    public void setL1i(int l1i) {
         this.l1i = l1i;
     }
     public int getL1i() {
         return l1i;
     }

    public void setL2(long l2) {
         this.l2 = l2;
     }
     public long getL2() {
         return l2;
     }

    public void setL3(long l3) {
         this.l3 = l3;
     }
     public long getL3() {
         return l3;
     }

}