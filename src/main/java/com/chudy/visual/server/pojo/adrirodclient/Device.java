/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.adrirodclient;
import java.util.List;
import java.util.Date;

/**
 * Auto-generated: 2019-07-18 18:7:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Device {

    private String imei;
    private List<String> imsi;
    private String model;
    private String vendor;
    private String uuid;
    public void setImei(String imei) {
         this.imei = imei;
     }
     public String getImei() {
         return imei;
     }

    public void setImsi(List<String> imsi) {
         this.imsi = imsi;
     }
     public List<String> getImsi() {
         return imsi;
     }

    public void setModel(String model) {
         this.model = model;
     }
     public String getModel() {
         return model;
     }

    public void setVendor(String vendor) {
         this.vendor = vendor;
     }
     public String getVendor() {
         return vendor;
     }

    public void setUuid(String uuid) {
         this.uuid = uuid;
     }
     public String getUuid() {
         return uuid;
     }

}