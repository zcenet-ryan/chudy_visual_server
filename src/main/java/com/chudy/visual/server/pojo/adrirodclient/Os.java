/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.adrirodclient;
import java.util.Date;

/**
 * Auto-generated: 2019-07-18 18:7:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Os {

    private String language;
    private String version;
    private String name;
    private String vendor;
    public void setLanguage(String language) {
         this.language = language;
     }
     public String getLanguage() {
         return language;
     }

    public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setVendor(String vendor) {
         this.vendor = vendor;
     }
     public String getVendor() {
         return vendor;
     }

}