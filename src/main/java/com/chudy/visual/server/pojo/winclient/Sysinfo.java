/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;
import java.util.List;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Sysinfo {

    private String version;
    private System system;
    private Bios bios;
    private Baseboard baseboard;
    private Chassis chassis;
    private Os os;
    private Uuid uuid;
    private Versions versions;
    private Cpu cpu;
    private Graphics graphics;
    private List<Net> net;
    private List<MemLayout> memLayout;
    private List<DiskLayout> diskLayout;
    private _customDevice _customDevice;
    public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }

    public void setSystem(System system) {
         this.system = system;
     }
     public System getSystem() {
         return system;
     }

    public void setBios(Bios bios) {
         this.bios = bios;
     }
     public Bios getBios() {
         return bios;
     }

    public void setBaseboard(Baseboard baseboard) {
         this.baseboard = baseboard;
     }
     public Baseboard getBaseboard() {
         return baseboard;
     }

    public void setChassis(Chassis chassis) {
         this.chassis = chassis;
     }
     public Chassis getChassis() {
         return chassis;
     }

    public void setOs(Os os) {
         this.os = os;
     }
     public Os getOs() {
         return os;
     }

    public void setUuid(Uuid uuid) {
         this.uuid = uuid;
     }
     public Uuid getUuid() {
         return uuid;
     }

    public void setVersions(Versions versions) {
         this.versions = versions;
     }
     public Versions getVersions() {
         return versions;
     }

    public void setCpu(Cpu cpu) {
         this.cpu = cpu;
     }
     public Cpu getCpu() {
         return cpu;
     }

    public void setGraphics(Graphics graphics) {
         this.graphics = graphics;
     }
     public Graphics getGraphics() {
         return graphics;
     }

    public void setNet(List<Net> net) {
         this.net = net;
     }
     public List<Net> getNet() {
         return net;
     }

    public void setMemLayout(List<MemLayout> memLayout) {
         this.memLayout = memLayout;
     }
     public List<MemLayout> getMemLayout() {
         return memLayout;
     }

    public void setDiskLayout(List<DiskLayout> diskLayout) {
         this.diskLayout = diskLayout;
     }
     public List<DiskLayout> getDiskLayout() {
         return diskLayout;
     }

    public void set_customDevice(_customDevice _customDevice) {
         this._customDevice = _customDevice;
     }
     public _customDevice get_customDevice() {
         return _customDevice;
     }

}