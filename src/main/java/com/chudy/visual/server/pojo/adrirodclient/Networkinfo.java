/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.adrirodclient;

/**
 * Auto-generated: 2019-07-18 18:7:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Networkinfo {

    private int currentType;
    private All all;
    private String mac;
    public void setCurrentType(int currentType) {
         this.currentType = currentType;
     }
     public int getCurrentType() {
         return currentType;
     }

    public void setAll(All all) {
         this.all = all;
     }
     public All getAll() {
         return all;
     }

    public void setMac(String mac) {
         this.mac = mac;
     }
     public String getMac() {
         return mac;
     }

}