/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;
import java.util.Date;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class MemLayout {

    private long size;
    private String bank;
    private String type;
    private int clockSpeed;
    private String formFactor;
    private String manufacturer;
    private String partNum;
    private String serialNum;
    private double voltageConfigured;
    private int voltageMin;
    private int voltageMax;
    public void setSize(long size) {
         this.size = size;
     }
     public long getSize() {
         return size;
     }

    public void setBank(String bank) {
         this.bank = bank;
     }
     public String getBank() {
         return bank;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setClockSpeed(int clockSpeed) {
         this.clockSpeed = clockSpeed;
     }
     public int getClockSpeed() {
         return clockSpeed;
     }

    public void setFormFactor(String formFactor) {
         this.formFactor = formFactor;
     }
     public String getFormFactor() {
         return formFactor;
     }

    public void setManufacturer(String manufacturer) {
         this.manufacturer = manufacturer;
     }
     public String getManufacturer() {
         return manufacturer;
     }

    public void setPartNum(String partNum) {
         this.partNum = partNum;
     }
     public String getPartNum() {
         return partNum;
     }

    public void setSerialNum(String serialNum) {
         this.serialNum = serialNum;
     }
     public String getSerialNum() {
         return serialNum;
     }

    public void setVoltageConfigured(double voltageConfigured) {
         this.voltageConfigured = voltageConfigured;
     }
     public double getVoltageConfigured() {
         return voltageConfigured;
     }

    public void setVoltageMin(int voltageMin) {
         this.voltageMin = voltageMin;
     }
     public int getVoltageMin() {
         return voltageMin;
     }

    public void setVoltageMax(int voltageMax) {
         this.voltageMax = voltageMax;
     }
     public int getVoltageMax() {
         return voltageMax;
     }

}