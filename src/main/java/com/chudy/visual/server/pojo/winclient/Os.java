/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Os {

    private String platform;
    private String distro;
    private String release;
    private String codename;
    private String kernel;
    private String arch;
    private String hostname;
    private String codepage;
    private String logofile;
    private String serial;
    private String build;
    private String servicepack;
    public void setPlatform(String platform) {
         this.platform = platform;
     }
     public String getPlatform() {
         return platform;
     }

    public void setDistro(String distro) {
         this.distro = distro;
     }
     public String getDistro() {
         return distro;
     }

    public void setRelease(String release) {
         this.release = release;
     }
     public String getRelease() {
         return release;
     }

    public void setCodename(String codename) {
         this.codename = codename;
     }
     public String getCodename() {
         return codename;
     }

    public void setKernel(String kernel) {
         this.kernel = kernel;
     }
     public String getKernel() {
         return kernel;
     }

    public void setArch(String arch) {
         this.arch = arch;
     }
     public String getArch() {
         return arch;
     }

    public void setHostname(String hostname) {
         this.hostname = hostname;
     }
     public String getHostname() {
         return hostname;
     }

    public void setCodepage(String codepage) {
         this.codepage = codepage;
     }
     public String getCodepage() {
         return codepage;
     }

    public void setLogofile(String logofile) {
         this.logofile = logofile;
     }
     public String getLogofile() {
         return logofile;
     }

    public void setSerial(String serial) {
         this.serial = serial;
     }
     public String getSerial() {
         return serial;
     }

    public void setBuild(String build) {
         this.build = build;
     }
     public String getBuild() {
         return build;
     }

    public void setServicepack(String servicepack) {
         this.servicepack = servicepack;
     }
     public String getServicepack() {
         return servicepack;
     }

}