/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Displays {

    private String model;
    private boolean main;
    private boolean builtin;
    private String connection;
    private int resolutionx;
    private int resolutiony;
    private int sizex;
    private int sizey;
    public void setModel(String model) {
         this.model = model;
     }
     public String getModel() {
         return model;
     }

    public void setMain(boolean main) {
         this.main = main;
     }
     public boolean getMain() {
         return main;
     }

    public void setBuiltin(boolean builtin) {
         this.builtin = builtin;
     }
     public boolean getBuiltin() {
         return builtin;
     }

    public void setConnection(String connection) {
         this.connection = connection;
     }
     public String getConnection() {
         return connection;
     }

    public void setResolutionx(int resolutionx) {
         this.resolutionx = resolutionx;
     }
     public int getResolutionx() {
         return resolutionx;
     }

    public void setResolutiony(int resolutiony) {
         this.resolutiony = resolutiony;
     }
     public int getResolutiony() {
         return resolutiony;
     }

    public void setSizex(int sizex) {
         this.sizex = sizex;
     }
     public int getSizex() {
         return sizex;
     }

    public void setSizey(int sizey) {
         this.sizey = sizey;
     }
     public int getSizey() {
         return sizey;
     }

}