package com.chudy.visual.server.generator;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.generator.MetaBuilder;

public class MetaBuilderExt extends MetaBuilder {

	protected String[] prefixes = {};
	public MetaBuilderExt(DataSource dataSource) {
		super(dataSource);
		// TODO Auto-generated constructor stub
	}
	public MetaBuilderExt(DataSource dataSource,String[] prefixes) {
		super(dataSource);
		this.prefixes = prefixes;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isSkipTable(String tableName) {
		// TODO Auto-generated method stub
		boolean flag = super.isSkipTable(tableName);
		if(null!=prefixes && prefixes.length>0){
			flag = true;//当配置了生成的表前缀时，设置默认为跳过，只有符合前缀的表才会被生
			for (int i = 0; i < prefixes.length; i++) {
				if(0==tableName.indexOf(prefixes[i])){
					flag = false;//不跳过，�?��生成
					break;
				}
			}
		}
		return flag;
	}
}
