package com.chudy.visual.server.service;

import com.chudy.visual.server.model.ScDevice;
import com.chudy.visual.server.pojo.adrirodclient.Sysinfo;
import com.chudy.visual.server.utils.SysInfoUtils;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;

import java.util.Date;

public class ScDeviceService {
	/**
	 * ���е� dao ����Ҳ���� Service �У���������Ϊ private������ sql ����� sql ֻ����ҵ��㣬���߷����ⲿ sql
	 * ģ�壬��ģ��������� http://www.jfinal.com/doc/5-13
	 */
	private ScDevice dao = new ScDevice().dao();

	public Page<ScDevice> paginate(int pageNumber, int pageSize) {
		return dao.paginate(pageNumber, pageSize, "select *", "from sc_device order by id asc");
	}

	public ScDevice findById(String id) {
		return dao.findById(id);
	}

	public void deleteById(String id) {
		dao.deleteById(id);
	}

	public void updateOnlineStatus(String deviceid, Integer status) {
		ScDevice device = new ScDevice();
		
		device.setId(deviceid);
		device.setOnlineStatus(status);
		
		System.out.println("---------------=" + JsonKit.toJson(device));
		device.update();
	}

	public ScDevice loginTenantDevice(String tenantCode,String pwd, ScDevice client) {
		ScDevice device = new ScDevice();
		//Db.queryFirst("","");
		return null;
	}


	public Ret retClientInfo(ScDevice client) {
		// 保存注册信息
		Ret ret = new Ret();
		ScDevice device = findById(client.getId());
		if (null != device && StrKit.notBlank(device.getId())) {
			// 获取到了注册信息
			device.setAppid(client.getAppid());
			device.setLocationId(client.getLocationId());
			device.setLocationName(client.getLocationName());
			device.setLocationRemark(client.getLocationRemark());
			device.setIp(client.getIp());
			if (SysInfoUtils.PlatForm_adrirod.equalsIgnoreCase(SysInfoUtils.getPlatform(client.getSysinfo()))) {
				// 安卓
				Sysinfo sysinfo = SysInfoUtils.fromAdrirodJson(client.getSysinfo());
				if (null != sysinfo) {
					device.setDeviceName(sysinfo.getDevice().getUuid());
					device.setOsType(SysInfoUtils.PlatForm_adrirod);
					device.setScreenResolution(sysinfo.get_customDevice().getDisplaysize().toString());
				}
			} else {
				// windows
				com.chudy.visual.server.pojo.winclient.Sysinfo sysinfo = SysInfoUtils.fromWinJson(client.getSysinfo());
				if (null != sysinfo) {
					device.setDeviceName(sysinfo.getSystem().getSku());
					device.setOsType(SysInfoUtils.PlatForm_win);
					device.setScreenResolution(sysinfo.get_customDevice().getDisplaysize().toString());
				}
			}

			device.setCreateTime(new Date());
			device.setOnlineStatus(1);//注册时，将设备在线状态初始为 在线
			device.setDelFlag(0);
			device.update();
			ret.put("code", "0");
			// 查询需要显示的内容
		} else {
			device = client;
			device.setAppid(client.getAppid());
			device.setLocationId(client.getLocationId());
			device.setLocationName(client.getLocationName());
			device.setLocationRemark(client.getLocationRemark());
			device.setIp(client.getIp());

			String platform = SysInfoUtils.getPlatform(client.getSysinfo());
			System.out.println("platform=" + platform);
			if (SysInfoUtils.PlatForm_adrirod.equalsIgnoreCase(platform)) {
				// 安卓
				Sysinfo sysinfo = SysInfoUtils.fromAdrirodJson(client.getSysinfo());
				if (null != sysinfo) {
					device.setDeviceName(sysinfo.get_customDevice().getDeviceid());
					device.setOsType(sysinfo.get_customDevice().getPlatform());
					device.setScreenResolution(sysinfo.get_customDevice().getDisplaysize().toString());
				}
			} else if (SysInfoUtils.PlatForm_win.equalsIgnoreCase(platform)) {
				// windows
				com.chudy.visual.server.pojo.winclient.Sysinfo sysinfo = SysInfoUtils.fromWinJson(client.getSysinfo());
				if (null != sysinfo) {
					device.setDeviceName(sysinfo.getSystem().getSku());
					device.setOsType(sysinfo.getOs().getPlatform());
					device.setScreenResolution(sysinfo.get_customDevice().getDisplaysize().toString());
				}
			}
			device.setCreateTime(new Date());
			device.setOnlineStatus(1);//注册时，将设备在线状态初始为 在线
			device.setDelFlag(0);
			device.save();
			// 未注册
			ret.put("code", "0");
		}
		return ret;
	}
}
