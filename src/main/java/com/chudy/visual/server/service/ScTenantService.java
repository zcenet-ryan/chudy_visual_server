package com.chudy.visual.server.service;

import cn.hutool.core.util.IdUtil;
import com.chudy.visual.server.model.ScDevice;
import com.chudy.visual.server.utils.IdUtils;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ScTenantService {

    /**
     * 设备首次注册前租户代码验证
     * @param tenantCode
     * @param pwd
     * @return
     */

    public Record verifyTenant(String tenantCode, String pwd) {
        Ret ret = new Ret();
        String sql = "select t.* from c_tenant t left join c_config c on t.id = c.tenant_id and t.status = 0 and t.del_flag=0 " +
                     "where t.subdomain = ? and c.value = ? and c.code='tenant_pwd'";
        Record record = Db.findFirst(sql,tenantCode,pwd);;
        return record;
    }


    /**
     *
     * @param tenantId
     * @return
     */
    public Ret initDepartSelectData(Long tenantId){
        Ret ret = new Ret();
        String sql = "select o.id from core_org o where o.del_flag = 0 and o.tenant_id = ? and o.parent_org_id is null";
        //根目录
        Record root = Db.findFirst(sql,tenantId);
        if(null != root){
            sql = "select o.name,o.id,o.parent_org_id as pId from core_org o where o.del_flag = 0 and o.tenant_id = ? and o.seq like ? order by o.id";
            List<Record> departsist = Db.find(sql,tenantId,"%."+root.getStr("id")+".%");
            if(null != departsist && 0 < departsist.size()){
                ret.set("data",departsist);
                ret.set("code","0");
                return ret;
            }
        }
        ret.set("data",new ArrayList());
        ret.set("msg","未查询到信息");
        return ret;
    }


    /**
     * 生成租户设备token，后续接口验证以token为准
     * @param device
     * @param tenantId
     * @return
     */
    public String createToken(ScDevice device, Long tenantId){
        //生成sessionkey，记录登录会话信息
        Record cSessionKey = new Record();
        cSessionKey.set("id", IdUtils.getId());
        cSessionKey.set("client_name",device.getDeviceName());
        cSessionKey.set("client_type",device.getOsType());
        cSessionKey.set("del_flag",0);
        cSessionKey.set("login_time",new Date().getTime()/1000);
        String sessionkey = IdUtil.simpleUUID();
        cSessionKey.set("sessionkey",sessionkey);
        cSessionKey.set("status",0);
        cSessionKey.set("tenant_id",tenantId);
        cSessionKey.set("imei",device.getId());
        //更新同设备ID 的sessionkey 为无效状态
        Db.update("update c_session_key set status = ?,update_time = ? where tenant_id = ? and imei = ?",1,new Date().getTime()/1000,tenantId,device.getId());
        //保存
        Db.save("c_session_key",cSessionKey);
        return sessionkey;
    }


    /**
     * 验证sessionKey
     * @param sessionkey
     * @param imei
     * @return
     */
    public Record verifyToken(String sessionkey,String imei){
        String sql = "select * from c_session_key k where k.sessionkey = ? and imei = ? and del_flag=0 and status=0";
        Record r = Db.findFirst(sql,sessionkey,imei);
        return r;
    }
}
