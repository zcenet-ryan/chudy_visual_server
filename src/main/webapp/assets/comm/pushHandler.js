var PushHandler = function () {
  this.onopen = function (event, ws) {
    // ws.send('hello 连上了哦')
    //document.getElementById('contentId').innerHTML += 'hello 连上了哦<br>';
	var msg = {
		errcode:'100000',
		errmsg:'open websocket'
	};
	ipcRenderer.send('app-socket-onlinemessage',msg )
  }

  /**
   * 收到服务器发来的消息
   * @param {*} event 
   * @param {*} ws 
   */
  this.onmessage = function (event, ws) {
    var data = event.data
    //document.getElementById('contentId').innerHTML += data + '<br>'
	var loadPage = {
				opurl:"http://www.baidu.com"
				//,				viewurl:"http://dajianv.com/"
	}
	console.log(data)
	if(typeof(data)=='string'){
		data = JSON.parse(data);
	}
	loadContentPage(data);
  }

  this.onclose = function (e, ws) {
    // error(e, ws)
  }

  this.onerror = function (e, ws) {
    // error(e, ws)
	console.log(e);
	//alert(e.message)
	//document.getElementById('contentId').innerHTML += '连不上了哦<br>';
	var msg = {
		errcode:'100500',
		errmsg:'close websocket'
	};
	ipcRenderer.send('app-socket-onlinemessage',msg )
  }

  /**
   * 发送心跳，本框架会自动定时调用该方法，请在该方法中发送心跳
   * @param {*} ws 
   */
  this.ping = function (ws) {
    // log("发心跳了")
    ws.send('<#heartbeat#>')
  }
}
