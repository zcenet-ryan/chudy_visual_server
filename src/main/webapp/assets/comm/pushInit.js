var ws_protocol = 'ws'; // ws 或 wss
var ws_ip = '127.0.0.1'
var ws_port = 9326

var heartbeatTimeout = 60000; // 心跳超时时间，单位：毫秒
var reconnInterval = 5000; // 重连间隔时间，单位：毫秒

var binaryType = 'blob'; // 'blob' or 'arraybuffer';//arraybuffer是字节
var handler = new PushHandler()

var tiows

function initScreenWs(params) {
	ws_protocol = params['ws_protocol']||ws_protocol;
	ws_ip = params['ws_ip']||ws_ip;
	ws_port = params['ws_port']||ws_port;
  // 参数，get方式传递
  var queryString = 'utype=screen'
  var param = "token=11&deviceid=adfadsf&userid=demo";
  tiows = new tio.ws(ws_protocol, ws_ip, ws_port, queryString, param, handler, heartbeatTimeout, reconnInterval, binaryType)
  tiows.connect()
}



function send () {
  var msg = document.getElementById('textId')
  tiows.send(msg.value)
}

function clearMsg () {
  document.getElementById('contentId').innerHTML = ''
}